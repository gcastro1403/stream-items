import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

class mainTest {
	
	main Main = new main();
	
	Item note = new Item(1,"note9", "Rosa", "Samsung", 5500);
	Item reno = new Item(2,"reno6", "Morado", "Oppo", 3000);
	Item redmi = new Item(3,"redmi", "verde", "Xiaomi", 1000);
	Item one = new Item(4,"one", "Amarillo", "Alcatel", 900);
	
	List<Item> itemsPhones = new ArrayList<Item>(Arrays.asList(note,reno,redmi,one));


	@Test
	void testFilterItems() {
		 List<Item> phone = main.filterItems( itemsPhones, "Alcatel" );
		 assertEquals(phone.get(0).getName(), "one");
	}
	
	
	@Test 
	void testexpensierItem() {
		Item phoneExpensive = main.expensierItem( itemsPhones);
		assertEquals( note, phoneExpensive );
	}
	
	
	@Test
	void testDescriptionItems() {
		List<Item> updateItem = main.descriptionItems(itemsPhones, "Xiaomi");
		assertEquals(updateItem.get(2).getDescription(), "the item was made by Xiaomi");
	}
	
	@Test
	void testPriceAllItems() {
		int result = main.priceAllItems(itemsPhones);
		assertEquals(result, 10400);
	}
	
	@Test
	void testFindItem() {
		boolean isMatch = main.findItem(itemsPhones, "reno6" );
		assertTrue(isMatch);
	}
	
	@Test
	void testFindItemFalse() {
		boolean isMatch = main.findItem(itemsPhones, "motorola" );
		assertFalse(isMatch);
	}
}
