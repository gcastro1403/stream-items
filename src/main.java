import java.util.Collections;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class main {
	
	
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Item barbie = new Item(1,"Barbie", "Rosa", "Mattel", 150);
		Item polly = new Item(2,"Polly", "Morado", "Mattel", 300);
		Item max = new Item(3,"Play-doh", "verde", "Hasbro", 80);
		Item pony = new Item(4,"Pony", "Amarillo", "Hasbro", 200);
		
		List<Item> itemsToys = new ArrayList<Item>(Arrays.asList(barbie,polly,max,pony));
		//filterItems(itemsToys);
		//expensierItem(itemsToys);
		//descriptionItems(itemsToys, "");
		//priceAllItems(itemsToys);
		//findItem(itemsToys, "");
		
	}
	
	
	//filter
	public static List<Item> filterItems (List<Item> list, String str) {
		List<Item> hasbroItems = list.stream().filter( (element) -> element.getManufacturer() == str)
		.collect(Collectors.toList());
		hasbroItems.forEach(System.out::println);
		return hasbroItems;
	}
	
	
	//sort
	public static Item expensierItem (List<Item> list) {
		list.sort((a,b) -> b.getPrice() - a.getPrice());
		return list.get(0);
		
	}
	
	
	//Map
	public static List<Item> descriptionItems(List<Item> list, String str) {
		List<Item> description = list.stream().map(element -> {
			if(element.getManufacturer() == str) {
				element.setDescription("the item was made by " + str);
			}
			return element;
		}).collect(Collectors.toList());
		description.forEach(System.out::println);
		return description;
	}
	
	//Reduce
	public static Integer priceAllItems (List<Item> list ) {
		int result = list.stream().reduce(0, (a, b) -> a + b.getPrice(), Integer::sum);
		System.out.println(result);
		return result;
	}
	
	//anyMatch 
	public static Boolean findItem (List<Item> list, String str ) {
		boolean isValid =  list.stream().anyMatch( element -> element.getName() == str );
		System.out.println(isValid);
		return isValid;
	}
	

}
