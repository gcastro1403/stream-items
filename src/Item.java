
public class Item {
	
	private Integer id;
	private String name;
	private String description;
	private String manufacturer;
	private Integer price;
	
	public Item(Integer id, String name, String description, String manufacturer, Integer price) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.manufacturer = manufacturer;
		this.price = price;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	
	
	@Override
    public String toString() {
        return "{" + this.name +", " +this.price +", "+ this.description + "}"  ;
    }

}
